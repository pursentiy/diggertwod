﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface WorldGenerator
{
    void GenerateWorld(int wide, int height);
}

