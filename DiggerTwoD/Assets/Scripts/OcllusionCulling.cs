﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OcllusionCulling : MonoBehaviour
{

    Camera _mainCamera;
    // Start is called before the first frame update
    void Start()
    {
        _mainCamera = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Debug.DrawRay(new Vector3(0, 0, 0), new Vector3(1, 1, 0) * 100f, Color.red, 10f);
    }
}
