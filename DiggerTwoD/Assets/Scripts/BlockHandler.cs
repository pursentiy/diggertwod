﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockHandler : MonoBehaviour
{
    GroundShadows _shadowManager;
    // Start is called before the first frame update
    void Start()
    {
        if (transform.tag == "TopLayer")
        {
            _shadowManager = FindObjectOfType<GroundShadows>();
            CheckAround(new Vector3(0f, -1f, 0f), "TopLayer", false);
        }
    }

    private void CheckAround(Vector3 direction, string tagName, bool setTeg)
    {
        float coef = 0;
        if (tagName == "TopLayer") coef = _shadowManager.darkCoeffTop;
        else if (tagName == "SideLayer") coef = _shadowManager.darkCoeffSide;

        RaycastHit2D[] hits = Physics2D.RaycastAll(transform.position,
                                                       transform.TransformDirection(direction),
                                                       (1 / coef) * FindObjectOfType<GameStarter>().span);
        if (hits.Length <= 0) return;
        for(int i = 0; i < hits.Length; i++)
        {
            _shadowManager.DarkenColor(i, hits[i].transform.GetComponent<BlockHandler>());
        }

    }

    public void OnDestroyBlock()
    {

    }
}
