﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStarter : MonoBehaviour
{
    [SerializeField] BlockHandler[] _tiles;
    [SerializeField] int _wide = 10;
    [SerializeField] int _maxHeight = 100;
    [SerializeField] int _minHeight = 0;
    [SerializeField] Transform _land;
    [SerializeField] int _rockLevel = 80;
    [SerializeField] int _oreLevel = 40;
    [SerializeField] int _landSwings = 5;
    [SerializeField] int _rockSwings = 5;

    private int _normalHeight = 100;
    private bool ifCanChangeHeight = true; 

    private int _span;
    public int span
    {
        get { return _span; }
        set { _span = value; }
    }



    // Start is called before the first frame update
    void Start()
    {
        span = 1;
        GenerateWorld();
    }

    // Update is called once per frame
    void Update()
    {
    }

    void GenerateWorld()
    {
        int currentHeight = 100;
        int targetHeight = 0;
        for (float x = _wide; x > 0; x -= _span)
        {
            SetLandscape(ref currentHeight, ref targetHeight);

            int rockLevel = (int)Random.Range(_rockLevel - _rockSwings, _rockLevel + _rockSwings);
            for (int y = _minHeight; y <= currentHeight; y++)
            {
                BlockHandler block;
                if (y == currentHeight)
                {
                    block = InstantiateBlock(x, y, _tiles[0]);
                    block.tag = "TopLayer";
                }
                else if (y >= rockLevel)
                {
                    block = InstantiateBlock(x, y, _tiles[1]);
                    //if (currentLevel < 1/_darkCoeff)
                    //{
                    //    DarkenColor(currentLevel, block);
                    //}
                    //else block.GetComponent<SpriteRenderer>().color = new Color(0f, 0f, 0f, 1f);
                }
                else
                {
                    block = InstantiateBlock(x, y, _tiles[3]);
                }
                if(y == _minHeight)
                {
                    block.tag = "LowerLayer";
                }
                block.GetComponent<SpriteRenderer>().color = new Color(0f, 0f, 0f, 1f);
            }
            if (currentHeight == targetHeight) ifCanChangeHeight = true;
            else ifCanChangeHeight = false;
        }
        ifCanChangeHeight = true;
    }

    private void SetLandscape(ref int currentHeight, ref int targetHeight)
    {
        if (ifCanChangeHeight)
        {
            targetHeight = (int)Random.Range(_normalHeight - _landSwings, _normalHeight + _landSwings);
        }

        if (Random.Range(0, 100) > 50)
        {
            if (currentHeight > targetHeight)
            {
                currentHeight--;
            }
            else
            {
                currentHeight++;
            }
        }
    }


    private BlockHandler InstantiateBlock(float x, float y, BlockHandler tile)
    {
        return Instantiate(tile, new Vector3(x, y, 0f), Quaternion.identity, _land);
    }
}
