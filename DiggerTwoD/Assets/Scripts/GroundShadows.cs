﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundShadows : MonoBehaviour
{
    private float _darkCoeffTop;
    public float darkCoeffTop
    {
        get { return _darkCoeffTop; }
        set { _darkCoeffTop = value; }
    }

    private float _darkCoeffSide;
    public float darkCoeffSide
    {
        get { return _darkCoeffSide; }
        set { _darkCoeffSide = value; }
    }

    private void Start()
    {
        darkCoeffTop = 0.2f;
        darkCoeffSide = 0.5f;
    }

    public void DarkenColor(int currentLevel, BlockHandler block)
    {
        Color blockColor = block.GetComponent<SpriteRenderer>().color;
        Color color = new Color(1 - (1 * currentLevel * _darkCoeffTop),
                                1 - (1 * currentLevel * _darkCoeffTop),
                                1 - (1 * currentLevel * _darkCoeffTop), 1f);
        block.GetComponent<SpriteRenderer>().color = color;
    }
}
